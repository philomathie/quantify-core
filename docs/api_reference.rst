.. jupyter-kernel:: python3
    :id: quantify_core_all_docs

quantify
=============

.. automodule:: quantify
   :members:

.. _analysis_api:

analysis
=============

.. automodule:: quantify.analysis
    :members:


base_analysis
-------------

.. automodule:: quantify.analysis.base_analysis
    :members:
    :show-inheritance:

cosine_analysis
---------------

.. automodule:: quantify.analysis.cosine_analysis
    :members:
    :show-inheritance:

spectroscopy_analysis
---------------------

.. automodule:: quantify.analysis.spectroscopy_analysis
    :members:
    :show-inheritance:

t1_analysis
-----------

.. automodule:: quantify.analysis.t1_analysis
    :members:
    :show-inheritance:

rabi_analysis
-------------

.. automodule:: quantify.analysis.rabi_analysis
    :members:
    :show-inheritance:

ramsey_analysis
---------------

.. automodule:: quantify.analysis.ramsey_analysis
    :members:
    :show-inheritance:

allxy_analysis
---------------

.. automodule:: quantify.analysis.allxy_analysis
    :members:
    :show-inheritance:

echo_analysis
---------------

.. automodule:: quantify.analysis.echo_analysis
    :members:
    :show-inheritance:

fitting_models
--------------

.. automodule:: quantify.analysis.fitting_models
    :members:
    :show-inheritance:


data
====

types
-----

.. automodule:: quantify.data.types
    :members:

handling
--------

.. automodule:: quantify.data.handling
    :members:


measurement
===========

.. automodule:: quantify.measurement
    :members:


utilities
=========

experiment_helpers
------------------

.. automodule:: quantify.utilities.experiment_helpers
    :members:


visualization
=============

.. automodule:: quantify.visualization
    :members:

color_utilities
---------------

.. automodule:: quantify.visualization.color_utilities
    :members:


mpl_plotting
------------

.. automodule:: quantify.visualization.mpl_plotting
    :members:

plot_interpolation
------------------

.. automodule:: quantify.visualization.plot_interpolation
    :members:


SI Utilities
------------

.. automodule:: quantify.visualization.SI_utilities
    :members:
